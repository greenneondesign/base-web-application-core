<?php
	
	/**
	 *	A simple model for stylesheet references
	 */
	class StyleSheetRef {
		public $url;
		public $media;
		
		public function __construct($url, $media = 'all') {
			$this->url = $url;
			$this->media = $media;
		}
	}
	
	/**
	 *	Theme in responsible for rendering our output in the requested manner.
	 *	HTML and JSON are currently supported.
	 */
	class Theme {
		private static $theme;	/** The singelton instance */
		
		/**
		 *	@var string the filesystem path fragment relative to the theme directory
		 *		where the resources for the currently selected theme in particular
		 *		header.php and footer.php can be found
		 */
		private $theme_path;

		public $site_title;
		public $copyright;
		public $include_in_navigation;
		
		public $stylesheets;
		public $scripts;
		public $global_classes;
		
		/**
		 *	Construct the theme, which is a combination of configuration settings and
		 *	arrays of stylesheets and scripts to reference in html output
		 */
		private function __construct() {
			$settings = \Application::application()->getConfigurationSection('theme');
			
			if(is_array($settings)) {
				if(array_key_exists('theme_directory', $settings) && array_key_exists('theme', $settings)) {
					$this->theme_path = $settings['theme_directory'] . DIRECTORY_SEPARATOR . $settings['theme'];
				} else if(array_key_exists('theme', $settings)) {	// default theme path but custom theme
					$this->theme_path = 'themes' . DIRECTORY_SEPARATOR . $settings['theme'];
				} else {
					$this->theme_path = 'themes' . DIRECTORY_SEPARATOR . 'Material';
				}
				
				if(array_key_exists('site_title', $settings)) {
					$this->site_title = $settings['site_title'];
				} else {
					$this->site_title = 'Base Web Application';
				}
				
				if(array_key_exists('copyright', $settings)) {
					$this->copyright = $settings['copyright'];
				} else {
					$this->copyright = null;
				}
				
				if(array_key_exists('include_in_navigation', $settings)) {
					$this->include_in_navigation = $settings['include_in_navigation'];
				} else {
					$this->include_in_navigation = null;
				}
			} else {
				$this->theme_path = 'themes' . DIRECTORY_SEPARATOR . 'Material';
				$this->site_title = 'Base Web Application';
			}

			// initialize some empty arrays to hold things
			$this->stylesheets = array();
			$this->scripts = array();
			$this->global_classes = array();
		}
		
		/**
		 *	theme - retrieves the shared theme instance
		 */
		public static function theme() {
			if(!self::$theme) {
				self::$theme = new Theme();
			}
			
			return self::$theme;
		}
		
		/**
		 *	enqueue_script - adds the url to a javascript file to the list of scripts
		 *		to be included with a <script> tag before the closing body tag of html
		 *		output
		 *
		 *	@param script - the url of a script that should be included in html output 
		 */
		public function enqueue_script($script) {
			if(!in_array($script, $this->scripts)) {
				$this->scripts[] = $script;
			}
		}
		
		/**
		 *	enqueue_style - adds the url to a css file to the list of stylesheets
		 *		to be included with a <link rel="stylesheet"> tag at the end of the
		 *		head element of html output
		 *
		 *	@param stylesheet - the url of a stylesheet that should be included in
		 *		html output 
		 */
		public function enqueue_stylesheet($url, $media = 'all') {
			foreach($this->stylesheets as $stylesheetref) {
				if(0 == strcasecmp($stylesheetref, $url)) {
					return; // stylesheet is already enqueued
				}
			}
			$this->stylesheets[] = new StyleSheetRef($url, $media);
		}
		
		/**
		 *	enqueue_global_class - adds a classname to the list of classnames to
		 *		apply to the body tag.
		 *
		 *	@param classname - the name of the class to add 
		 */
		public function enqueue_global_class($classname) {
			if(!in_array($classname, $this->global_classes)) {
				$this->global_classes[] = $classname;
			}
		}

		/**
		 *	render_static - outputs the provided page content after wrapping it in the
		 *		theme elements provided by the configured theme
		 *
		 *	@param partial - the name of the file containing the page content
		 *	@param basepath - the directory name relative to the document root
		 *		containing the partial file. Defaults to the web application default
		 *		static pages directory.
		 */
		public function render_static($partial, $basepath = NULL) {
			if(NULL === $basepath) {
				$basepath = \Application::application()->getStaticPartialsDirectory();
			}
			
			$content_file_path = $basepath . DIRECTORY_SEPARATOR . $partial;
			if(!file_exists($content_file_path)) {
				throw new NotFoundException();
			}
			
			// begin output
			include_once($this->theme_path . DIRECTORY_SEPARATOR . 'header.php');
		
			// pull in content
			include($content_file_path);
			
			// do notifications
			Notification::report();
		
			// finish
			include_once($this->theme_path . DIRECTORY_SEPARATOR . 'footer.php');
		}

		/**
		 *	render_error - outputs an error page after wrapping it in the
		 *		theme elements provan error page
		 *
		 *	@param partial - the name of the file containing the page content
		 *	@param basepath - the directory name relative to the document root
		 *		containing the partial file. Defaults to the web application default
		 *		static pages directory.
		 */
		public function render_error($partial, $error) {
			$basepath = \Application::application()->getErrorPartialsDirectory();
			
			$content_file_path = $basepath . DIRECTORY_SEPARATOR . $partial;
			if(!file_exists($content_file_path)) {
				throw new NotFoundException();	// probably can't at this point
			}
			
			// begin output
			include_once($this->theme_path . DIRECTORY_SEPARATOR . 'header.php');
		
			// pull in content
			include($content_file_path);
			
			// do notifications
			Notification::report();
		
			// finish
			include_once($this->theme_path . DIRECTORY_SEPARATOR . 'footer.php');
		}

		/**
		 *	render - outputs the provided page content
		 *
		 *	@param partial - the name of the file containing a template to format the
		 *		content as html. Must be within the calling modules' bundle
		 *	@param context - an associative array containing the data needed by the 
		 *		specified template
		 */
		public function render($partial, $context = NULL) {
			$requested_response_type = $_SERVER['HTTP_ACCEPT'];
			switch($requested_response_type) {
				case 'application/json':
					echo json_encode($context);
					break;
				case 'text/html':
				default:
					$template_file_path = 'modules' . DIRECTORY_SEPARATOR . Application::application()->module . DIRECTORY_SEPARATOR . $partial;
					if(!file_exists($template_file_path)) {
						throw new NotFoundException();
					}
					
					// begin output
					include_once($this->theme_path . DIRECTORY_SEPARATOR . 'header.php');
		
					// unpack context... this is dangerous a malicious template could overwrite variables in the local stack frame 
					if(is_array($context)) {
						foreach($context as $key => $value) { $$key = $value; }					
					}
		
					// pull in content
					include($template_file_path);
		
					// do notifications
					Notification::report();
	
					// finish
					include_once($this->theme_path . DIRECTORY_SEPARATOR . 'footer.php');
			}
		}
		
		/**
		 *	A helper to create similarly structured pagination links sitewide
		 *
		 *	@param base_url - the part of the url before the query string. Typically,
		 *		it is '/' + the module name + '/' for the restful object list but it
		 *		could be module and an action name
		 *	@param query - the query string to prepend before the pagination query parts
		 *	@param total_search_results - the count of the total number of entries
		 *		that could be displayed
		 *	@param page_size - an integer representing how many results to show
		 *		on a page
		 *	@param page - an integer representing what page we are displaying
		 */
		public function pagination_links($base_url, $query, $total_search_results, $page_size, $page) {
			if(NULL != $page_size && $page_size > 0) {	//catch div by zero issues early
				$query_fragments = array();
				if(isset($_GET['sort']) && !empty($_GET['sort'])) {
					$query_fragments[] = 'sort=' . strip_tags($_GET['sort']);
				}

				if(isset($_GET['order']) && !empty($_GET['order'])) {
					$query_fragments[] = 'order=' . strip_tags($_GET['order']);
				}

				if(isset($_GET['results_per_page']) && !empty($_GET['results_per_page'])) {
					$query_fragments[] = 'results_per_page=' . strip_tags($_GET['results_per_page']);
				}

				if(count($query_fragments) > 0) {
					if(NULL != $query) {
						$base_query = $query . '&' . join('&', $query_fragments);
					} else {
						$base_query = join('&', $query_fragments);
					}
				} else {
					if(NULL != $query) {
						$base_query = $query;
					} else {
						$base_query = '';
					}
				}

				$page_count = intval(ceil($total_search_results / $page_size));

				include_once($this->theme_path . DIRECTORY_SEPARATOR . 'pagination-links.php');
			}
		}
		
		/**
		 *	A very crude way of breaking text that may be multiline and formating it
		 *	for inclusion in html.
		 *
		 *	@param text - the text to format
		 *	@param indent - a number of tab characters to insert before each line of
		 *		output. Defaults to 0
		 */
		public function formatTextAsHTML($text, $indent = 0) {
			$paragraphs = preg_split('/\R/', $text, -1, PREG_SPLIT_NO_EMPTY);
			$leader = '';
			for($i=0; $i<$indent; $i++) {
				$leader .= '	';
			}
			foreach($paragraphs as $paragraph) {
				// detect urls and turn into links
				$paragraph = preg_replace('((?:|http:|https:)//[a-zA-Z0-9.%&?:=/]+)', '<a href="${0}">${0}</a>', $paragraph);
				echo $leader . '<p>' . $paragraph . "</p>\n";
			}
		}
	}
?>