<?php
	
	/**
	 *	Cross Site Request Forgeries are a vulnerability where a attacker tricks
	 *	a user authenticated with a web application to trigger a link that has
	 *	undesirable consequences such as deleting an object from the database. To
	 *	foil these attacks we can store a random value in the Session and place
	 *	a hidden input in POST/PUT etc. forms with a value that is derived in a one
	 *	way fashion (salted hash) from the secret value. Then when we receive
	 *	input that seems to be from a POST/PUT form we can rederive the value and
	 *	check that that value was supplied.
	 *
	 *	Security note: we need to choose a new secret value every time the user
	 *	logs in. We handle this by unsetting the secret when the user logs out
	 *	in \User\ViewController. 
	 */
	class CSRF {
		
		/**
		 *	@return true iff a form POSTs a csrf token derived from the csrf secret
		 *		stored in the session and false otherwise.
		 */
		public static function validate() {
			if(session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['csrf_secret'], $_POST['csrf_token']) && !empty($_POST['csrf_token'])) {
				return password_verify($_SESSION['csrf_secret'], $_POST['csrf_token']);
			}
			
			return false;
		}
		
		/**
		 *	emit - adds a hidden HTML form field to the output stream with a token
		 *		derived from the csrf_secret stored in the session
		 */
		public static function emit() {
			if(session_status() !== PHP_SESSION_ACTIVE) {
				throw new InternalServerError();
			}
				
			if(!isset($_SESSION['csrf_secret'])) {
				$_SESSION['csrf_secret'] = CSRF::generateToken();
			}
			
			$token = password_hash($_SESSION['csrf_secret'], PASSWORD_DEFAULT);
			
			echo '<input type="hidden" name="csrf_token" value="' . $token . "\">\n";
		}
		
		/**
		 *	Create a new random token
		 */
		private static function generateToken() {
			// DANGER HARDCODED VALUES!!!
			$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
			$length = 20;
			
			$len = strlen($chars)-1;
			$pw = '';
 
			for($i=0;$i<$length;$i++) {
				$pw .= $chars[rand(0, $len)];
			}
 
			return str_shuffle($pw);
		}
	}
?>