<?php
	
	/**
	 *	DBSessionHandler uses our DB wrapper around PDO to store session data in
	 *	our database. This component is not needed by the rest of Base Web
	 *	Application but can be helpful when running in certain web hosting
	 *	environments... e.g. shared hosting without file based sessions and
	 *	multi-server hosting
	 */
	class DBSessionHandler implements SessionHandlerInterface {
		private $connection;

		public function close() {
			return true;
		}

		public function destroy($session_id) {
			$sql = 'DELETE FROM sessions WHERE id = :id';
			$query = $this->connection->prepare($sql);
			$query->bindValue(':id', $session_id);
			if($query->execute()) {
				return true;
			}
			
			return false;
		}

		public function gc($maxlifetime) {
			$sql = 'DELETE FROM sessions WHERE now() - last_access > :max_lifetime';
			$query = $this->connection->prepare($sql);
			$query->bindValue(':max_lifetime', "interval '" . $maxlifetime . "seconds'");
			if($query->execute()) {
				return true;
			}
			
			return false;
		}

		public function open($save_path, $session_name) {
			$this->connection = \DB::getConnection();
			if($this->connection) {
				return true;
			} else {
				throw new \InternalServerError("Unable to connect to the database");
			}
		}
		
		public function read($session_id) {
			$sql = 'SELECT data FROM sessions WHERE id = :id';
			$query = $this->connection->prepare($sql);
			$query->bindValue(':id', $session_id);
			if($query->execute()) {
				if($row = $query->fetch(\PDO::FETCH_ASSOC)) {
					if(!empty($row['data'])) {	// php7 is strict about recieving a string
												// but when data is empty php doesn't type it as a string
												// casting to string is unavailable in php5 hence this code
						return $row['data'];
					} else {
						return '';
					}
				} else {	//	query failed... could be the record was cleared from the
							//	database or perhaps a new id was picked that does not exist
							//	in the database
					return '';
				}
			}
			
			return '';
		}

		public function write($session_id, $session_data) {
			switch($this->connection->driver) {
				case 'mysql':
					$sql = 'INSERT INTO sessions (id, data) VALUES (:id, :data) ON DUPLICATE KEY UPDATE data=:data, last_access = now()';
					break;
				case 'pgsql':
					$sql = 'INSERT INTO sessions (id, data) VALUES (:id, :data) ON CONFLICT (id) DO UPDATE SET data = :data, last_access = now() WHERE sessions.id = :id';
					break;
				default:
					return false;
			}
			
			$query = $this->connection->prepare($sql);
			$query->bindValue(':id', $session_id);
			$query->bindValue(':data', $session_data);
			if($query->execute()) {
				return true;
			}
			
			return false;
		}
	}
	
	session_set_save_handler(new DBSessionHandler(), true);
?>