<?php

	/**
	 *	DB is a singelton wrapper around PDO that injects settings from the
	 *	config.ini file. A .htaccess should be used to protect the directory
	 *	containing the config.ini file.
	 */
	class DB extends PDO {
		private static $pdo;	/** The singelton instance */

		public $driver;	/** In order to do UPSERTs we need to customize query string
							for the driver but that means being able to determine
							the driver. yuck */
		
		/**
		 *	__construct - reads the specified config file or if one is not specified
		 *		looks for a file named 'config.ini' in the include path. Using the
		 *		settings in the config file it then creates a PDO database instance
		 *
		 *	@param file - the name of the config file to read. defaults to
		 *		'core/config.ini'
		 *
		 *	@sideeffect - if the config file can not be found and read, script
		 *		execution will end abnormally
		 */
		public function __construct() {
			$settings = \Application::application()->getConfigurationSection('database');
			if($settings != NULL) {
				$this->driver = $settings['driver'];
				switch($this->driver) {
					case 'mysql':
					case 'pgsql':
						// should throw exception if host, database or user keys are missing
						// should support no password if key is not provided
						$dsn = 'host=' . $settings['host'] . ';dbname=' . $settings['database'];
						parent::__construct($settings['driver'] . ':' . $dsn, $settings['username'], $settings['password']);
						break;
					case 'sqlite':
						parent::__construct($settings['driver'] . ':' . $settings['filename']);
						break;
					default:
						// throw exception
						die('Unsupported database connection requested');
				}
			} else {
				// throw exception
				die('Unable to read settings from config file');
			}
		}
		
		/**
		 *	getConnection - accessor to the PDO singleton
		 *
		 *	@return - the singleton Database connection as a PDO subclass instance
		 */
		public static function getConnection() {
			if(!self::$pdo) {
				try {
					self::$pdo = new DB();
				} catch(PDOException $e) {
					return false;
				}
			}
			
			return self::$pdo;
		}
	}
?>