				<article>
					<h1>That didn't work</h1>
<?php	if(!empty($error)): ?>
					<p><?=$error ?></p>
<?php	else: ?>
					<p>Unfortunately something bad happened and we could not receive the necessary information to complete your request. Perhaps try again? However if you keep seeing this error you will likely want to contact support and describe what you were trying to do so they can trouble shoot the problem</p>
<?php	endif; ?>
				</article>
