<?php
	// pull in a few core classes; essentially autoload core but without the overhead
	include_once($_SERVER['DOCUMENT_ROOT'] . '/core/Database/Database.php');
	include_once($_SERVER['DOCUMENT_ROOT'] . '/core/Notification/Notification.php');
	include_once($_SERVER['DOCUMENT_ROOT'] . '/core/Theme/Theme.php');
	
	// define some Exceptions
	class BadRequestException extends Exception {  }
	class NotFoundException extends Exception {  }
	class NotAuthorizedException extends Exception {  }
	class InternalServerError extends Exception {  }
	
	// define ViewController interface
	interface ViewController {
		/**
		 *	_navigation_slug returns the text representation for a link to the default
		 *		(automatic) action of this module. Used when dynamically creating
		 *		navigation elements
		 *
		 *	@return - text for a link to this module's default action
		 */
		public static function _navigation_slug();
		
		/**
		 *	_visible indicates whether it is reasonable for this module to appear in
		 *		navigation elements
		 *
		 *	@return - boolean true if this module's default action should be present
		 *		in navigation elements and false otherwise.
		 */
		public static function _visible();
		
		/**
		 *	automatic is the default action/entry for any ViewController. To be called
		 *		when the request URI indicates the module and nothing else. Typically,
		 *		presents a list or search view.
		 */
		public static function automatic();
	}

	/**
	 *	The Application is the heart of things. It turns our restful urls into
	 *	method calls. A single instance is created making the constructor a sane
	 *	place to read configuration. Note Database also reads the configuration
	 */
	class Application {
		/** @var Application The singelton instance */
		private static $application;

		/**
		 *	@var array An associative array, aka dictionary of option read from the
		 *		configuration file
		 */
		private $configuration;
		
		/**
		 *	@var string A url path fragment to a valid resource that the application
		 *		should render when no path fragment is otherwise provided
		 */
		private $default_route;
		
		/**
		 *	@var string A filesystem path relative to $_SERVER['DOCUMENT_ROOT'] where
		 *		the partials for rendering error pages can be found
		 */
		private $error_partials_directory;
		
		/**
		 *	@var string A filesystem path relative to $_SERVER['DOCUMENT_ROOT'] where
		 *		the content partials for rendering static content pages can be found
		 */
		private $static_partials_directory;
		
		/**
		 *	@var array A list of all the static pages that the application is aware of
		 *		i.e. can route requests to
		 */
		private $static_pages;
		
		public $module;
		public $routes;
		public $hostname;	//	$_SERVER['hostname'] can be spoofed so we need to read from config
							//		the desired value and expose it here
		
		/**
		 *	__construct - constructs an application instance, reading the
		 *		configuration, starting the session, enumerating static routes
		 *		then loading modules
		 */
		private function __construct() {
			// read our configuration
			$settings_file = $_SERVER['DOCUMENT_ROOT'] . '/config/config.json';
			if(file_exists($settings_file)) {
				$this->configuration = json_decode(file_get_contents($settings_file), TRUE);
				if(NULL === $this->configuration) {
					die('Unable to read configuration');
				}
			}
			
			if(isset($this->configuration['core'])) {
				// set some development options
				if(isset($this->configuration['core']['development']) && $this->configuration['core']['development']) {
					ini_set('display_errors', '1');
				}
				if(isset($this->configuration['core']['session_use_db_storage']) && $this->configuration['core']['session_use_db_storage']) {
					include_once($_SERVER['DOCUMENT_ROOT'] . '/core/Session/DBSessionHandler.php');
				} else {
					if(isset($this->configuration['core']['session_file_path'])) {
						session_save_path($this->configuration['core']['session_file_path']);
					}
				}
				
				if(isset($this->configuration['core']['hostname'])) {
					$this->hostname = $this->configuration['core']['hostname'];
				} else {
					$this->hostname = 'localhost';
				}
				
				// setup some paths to partials
				if(isset($this->configuration['core']['error_partials_directory'])) {
					$this->error_partials_directory = $this->configuration['core']['error_partials_directory'];
				} else {
					$this->error_partials_directory = $_SERVER['DOCUMENT_ROOT'] . '/core/Application/error';
				}
				
				if(isset($this->configuration['core']['static_partials_directory'])) {
					$this->static_partials_directory = $this->configuration['core']['static_partials_directory'];
				} else {
					$this->static_partials_directory = $_SERVER['DOCUMENT_ROOT'] . '/core/Application/static';
				}
				
				if(isset($this->configuration['core']['use_csrf']) && $this->configuration['core']['use_csrf']) {
					include_once($_SERVER['DOCUMENT_ROOT'] . '/core/Session/CSRF.php');
				}
			} else {
				die('The configuration file is corrupted or incomplete');
			}
			
			// set the default route
			if(isset($this->configuration['routes']) && isset($this->configuration['routes']['default_route'])) {
				$this->default_route = $this->configuration['routes']['default_route'];
			} else {
				die('The configuration file is corrupted or incomplete');
			}
			
			// enumerate the static pages
			$this->static_pages = array_filter(scandir($this->static_partials_directory), function ($item) {
				return 0 !== strpos($item, '.');
			});
			
			// load the modules
			$modules = array_filter(scandir($_SERVER['DOCUMENT_ROOT'] . '/modules'), function ($item) {
				return 0 !== strpos($item, '.');
			});
			// enumerate our routes provided by our modules
			$this->routes = array();
			foreach($modules as $module) {
				$view_controller_file_path = $_SERVER['DOCUMENT_ROOT'] . '/modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'ViewController.php';
				if(is_file($view_controller_file_path)) {
					if(include_once($view_controller_file_path)) {
						$view_controller = $module . '\ViewController';
						if(class_exists($view_controller)) {
							$reflection = new ReflectionClass($view_controller);
							if($reflection->isSubclassOf('\ViewController')) {
								$this->routes[strtolower($module)] = $module;
							}
						}
					}					
				}
			}
		}
		
		/**
		 *	Performs deferred startup tasks that have to wait until the application
		 *	instance exists.
		 */
		public function startup() {
			// session requires User class to decode properly and may require the DB to
			//		be loaded and configured or session save path which may be relocated
			//		based on the configuration all of which happens in the constructor
			if(session_status() !== PHP_SESSION_ACTIVE) {
				try {
					$success = @session_start();
					if(!$success) {
						throw new \InternalServerError("Unable to read from session storage");
					}
				} catch(\InternalServerError $ise) {
					$this->render_internal_server_error($ise);
					session_abort();
					exit();	//prematurly end execution
				}
			}
			
			$this->route($_SERVER['REQUEST_URI']);
		}
		
		/**
		 *	application - retrieves the shared application instance
		 */
		public static function application() {
			if(!self::$application) {
				self::$application = new Application();
			}
			
			return self::$application;
		}
		
		/**
		 *	Get the configuration settings in the specified named section
		 *
		 *	@param {string} section - the name of the section (top level key) in
		 *		the configuration file to retrieve the values under. In the future
		 *		this should become a path based lookup.
		 *
		 *	@return array|null an associative array aka dictionary of configuration
		 *		options in the top level section named
		 */
		public function getConfigurationSection($section = NULL) {
			if(NULL === $section || !array_key_exists($section, $this->configuration)) {
				return NULL;
			}
			
			return $this->configuration[$section];
		}
		
		public function getErrorPartialsDirectory() {
			return $this->error_partials_directory;
		}
		
		public function getStaticPartialsDirectory() {
			return $this->static_partials_directory;
		}
		
		// rendering
		private function render_bad_request($exception) {
			header('HTTP/1.0 400 Bad Request');
			Theme::theme()->render_error('400.php', $exception->getMessage());
		}
		
		private function render_not_found($exception) {
			header('HTTP/1.0 404 Not Found');
			Theme::theme()->render_error('404.php', $exception->getMessage());
		}
		
		private function render_not_authorized($exception) {
			header('HTTP/1.0 403 Not Authorized');
			if(Users\User::is_user_authenticated()) {
				Theme::theme()->render_error('403.php', $exception->getMessage());
			} else {
				$this->redirect('/users/login');
			}
		}

		private function render_internal_server_error($exception) {
			header('HTTP/1.0 500 Internal Server Error');
			Theme::theme()->render_error('500.php', $exception->getMessage());
		}
		
		/**
		 *	Sender the User agent to a new location. Should be used by ViewControllers
		 *	after the successful processing of a POS request so the user doesn't reload
		 *	and thus repost.
		 *
		 *	@param $destination - the location to redirect to. Remember to include a
		 *		leading '/' otherwise the redirect is relative to the current location
		 */
		public function redirect($destination = NULL) {
			if(NULL === $destination) {
				$destination = '/' . $this->default_route;	// Location without leading '/'
															// is relative to present
															// location
			}
			
			header('HTTP/1.1 303 See Other');
			header('Location: ' . $destination);
		}
		
		/**
		 *	route takes a path, initially the REQUEST_URI and turns it into a method
		 *	dispatch. Notably route may in certain circumstances call itself with a
		 *	different path perhaps because the initial path caused a non fatal error
		 *	to occur and the application needs to fallback to a sane default. Note
		 *	no effort has been made to catch infinite recursion... we just exhaust
		 *	the runtime stack space or hit the execution time limit.
		 *
		 *	@param $path - a URI style resource name as a '/' delimited path
		 */
		public function route($path = NULL) {
			// cut off query string if exists
			$query_offset = strpos($path, '?');
			if($query_offset !== FALSE) {
				$path = substr($path, 0, $query_offset);
			}
			
			// break path apart and process assuming the form; /[collection]/[verb]/[object_id]
			$fragments = explode('/', trim($path, '/'));
			if($fragments === FALSE) {	// explode of an empty string returns false... render homepage
				$this->route($this->default_route);
			} else {	// explode should have returned an array with at least one value in it (our limit is non negative)
				if(NULL === $fragments[0] || empty($fragments[0])) {		// but sometimes that element is null or empty
					$this->route($this->default_route);
				} else if(in_array($fragments[0], $this->static_pages)) {	// a static route
					Theme::theme()->render_static($fragments[0]);
				} else if(array_key_exists($fragments[0], $this->routes)) {	// a dynamic route
					$this->module = $this->routes[$fragments[0]];
					$delegate = $this->routes[$fragments[0]] . '\ViewController';
					try {
						if(count($fragments) > 1) {	//there is another token. it ought to be the verb
							$verb = $fragments[1];
							if($verb[0] === '_' || !is_callable(array($delegate,$verb))) {
								throw new NotFoundException();
							}
							if(count($fragments) > 2) { // there is another token it ought to be the object or it's id
								$object = $fragments[2];
								$delegate::$verb($object);
							} else {	// no object call method with no params
								$delegate::$verb();
							}
						} else {	// no verb call automatic
							$delegate::automatic();
						}
					} catch(BadRequestException $bre) {
						$this->render_bad_request($bre);
					} catch(NotAuthorizedException $nae) {
						$this->render_not_authorized($nae);
					} catch(NotFoundException $nfe) {
						$this->render_not_found($nfe);
					}  catch(InternalServerError $ise) {
						$this->render_internal_server_error($ise);
					}
				} else {	// no matching route was registered... render page not found
					$this->render_not_found(new NotFoundException());
				}
			}
		}
	}

	Application::application()->startup();
?>