<?php
	/**
	 *	Notification serves as a sort of in memory, but persistent across redirects
	 *	means of presenting notifications to the viewer. This is achieved by
	 *	maintaining a queue in the $_SESSION to be persistant across redirects but
	 *	remain storage implementation agnostic.
	 */
	class Notification {
		const NOTICE = 'notice';
		const ERROR = 'error';
		const SUCCESS = 'success';
		
		/**
		 *	log - enqueues a message to be displayed to the user on next page load
		 *		with the level of "notice"
		 *
		 *	@param message - the text to report to the user
		 */
		public static function log($message) {
			Notification::enqueue(self::NOTICE, $message);
		}
		
		/**
		 *	log - enqueues a message to be displayed to the user on next page load
		 *		with the level of "error"
		 *
		 *	@param message - the text to report to the user
		 */
		public static function error($message) {
			Notification::enqueue(self::ERROR, $message);
		}
		
		/**
		 *	success - enqueues a message to be displayed to the user on next page load
		 *		with the level of "success"
		 *
		 *	@param message - the text to report to the user
		 */
		public static function success($message) {
			Notification::enqueue(self::SUCCESS, $message);
		}
		
		/**
		 *	enqueue - adds a notification to be displayed to the user on the next page
		 *		load with the specified level and message text
		 *
		 *	@param level - typically one of the predefined constants
		 *	@param message - the text to report to the user
		 */
		public static function enqueue($level, $message) {
			if(!isset($_SESSION['notifications'])) {
				$_SESSION['notifications'] = array();
			}
			$_SESSION['notifications'][] = new Notification($level, $message);
		}
		
		/**
		 *	report - if there are messages in the queue, displays them to the viewer
		 */
		public static function report() {
			if(!isset($_SESSION['notifications']) || count($_SESSION['notifications']) <= 0)
				return;

			include('views/report.php');
			unset($_SESSION['notifications']);
		}
		
		/**
		 *	Create a new Notification
		 */
		private function __construct($level, $message) {
			$this->level = $level;
			$this->message = $message;
		}
	}
?>